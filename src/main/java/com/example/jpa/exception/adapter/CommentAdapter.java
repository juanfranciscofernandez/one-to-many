package com.example.jpa.exception.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.jpa.exception.dto.CommentDTO;
import com.example.jpa.model.Comment;

@Component
public class CommentAdapter {
	
	@Autowired
	private PostAdapter postAdapter;

	public List<CommentDTO> convertList2ListDTO(List<Comment> listaComentarios) {
		List<CommentDTO> listCommentDTO = new ArrayList<CommentDTO>();
		for (Comment comment : listaComentarios) {
			CommentDTO commentDTO = new CommentDTO();
			commentDTO.setId(comment.getId());
			commentDTO.setText(comment.getText());
			commentDTO.setPostDTO(postAdapter.convertPost2PostDTO(comment.getPost()));
			listCommentDTO.add(commentDTO);
		}
		return listCommentDTO;
	}

}
