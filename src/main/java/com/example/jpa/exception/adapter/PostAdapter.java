package com.example.jpa.exception.adapter;

import org.springframework.stereotype.Component;

import com.example.jpa.exception.dto.PostDTO;
import com.example.jpa.model.Post;

@Component
public class PostAdapter {

	public PostDTO convertPost2PostDTO(Post post) {
		PostDTO postDTO = new PostDTO();
		postDTO.setId(post.getId());
		postDTO.setContent(post.getContent());
		postDTO.setDescription(post.getDescription());
		postDTO.setTitle(post.getTitle());
		return postDTO;
	}

}
