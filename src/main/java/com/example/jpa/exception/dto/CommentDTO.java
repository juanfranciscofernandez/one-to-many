package com.example.jpa.exception.dto;

public class CommentDTO {
	
    private Long id;
    
    private String text;

    private PostDTO postDTO;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public PostDTO getPostDTO() {
		return postDTO;
	}

	public void setPostDTO(PostDTO postDTO) {
		this.postDTO = postDTO;
	}

	@Override
	public String toString() {
		return "CommentDTO [id=" + id + ", text=" + text + ", postDTO=" + postDTO + "]";
	}
    
    
}
