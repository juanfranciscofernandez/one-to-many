package com.example.jpa.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.jpa.exception.adapter.CommentAdapter;
import com.example.jpa.exception.dto.CommentDTO;
import com.example.jpa.model.Comment;
import com.example.jpa.repository.CommentRepository;

@RestController
public class CommentController {

	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private CommentAdapter commentAdapter;

	@GetMapping("/comments")
	public List<CommentDTO> getAllComments(Pageable pageable) {
		List<Comment> listaComentarios = new ArrayList<Comment>();

		for (Comment comment : commentRepository.findAll()) {
			Comment comment1 = new Comment();
			comment.setPost(comment.getPost());
			listaComentarios.add(comment);
		}
		
		return commentAdapter.convertList2ListDTO(listaComentarios);
		
	}

	@GetMapping("/posts/{postId}/comments")
	public Page<Comment> getAllCommentsByPostId(@PathVariable(value = "postId") Long postId, Pageable pageable) {
		return commentRepository.findByPostId(postId, pageable);
	}

}
